<?php

// This file is auto-generated, don't edit it. Thanks.
namespace nuke2015\api\org;

// "alibabacloud/client": "^1.5", --composer.json
use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use nuke2015\api\config as myconf;

class sms
{

    // sendSMS('135****','SMS_*****',[]);
    public static function sendSMS($phone, $tplCode, $tplParam)
    {
        // 配置
        $config = myconf\params::aliyun_dayu();

        // 发送
        AlibabaCloud::accessKeyClient($config['accessKeyId'], $config['accessKeySecret'])
            ->regionId('cn-hangzhou')->asGlobalClient();

        try {
            $query = [
                'PhoneNumbers' => $phone,
                'SignName'     => $config['SignName'],
                'TemplateCode' => $tplCode,
            ];
            // 参数
            if ($tplParam && count($tplParam)) {
                $query['TemplateParam'] = json_encode($tplParam, 320);
            }
            // var_dump($query);exit;
            $result = AlibabaCloud::rpcRequest()
                ->product('Dysmsapi')
            // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->options([
                    'query' => $query,
                ])
                ->request();

            $result = $result->toArray();
            if ($result['Code'] != 'OK') {
                switch ($result['Code']) {
                    case 'isv.DAY_LIMIT_CONTROL':
                        return '发送次数超过限制';
                    case 'isv.SMS_CONTENT_ILLEGAL':
                        return '短信包含';
                    case 'isv.SMS_SIGN_ILLEGAL':
                        return '签名禁止使用';
                    case 'isv.OUT_OF_SERVICE':
                        return '短信业务暂不可用';
                    case 'isv.INVALID_PARAMETERS':
                        return '短信参数异常';
                    case 'isv.isp.SYSTEM_ERROR':
                        return '请重试';
                    case 'isv.MOBILE_NUMBER_ILLEGAL':
                        return '非法手机号';
                    case 'isv.AMOUNT_NOT_ENOUGH':
                        return '短信欠费!';
                    default:
                        return $result['Code'];
                }
            }
        } catch (ClientException $e) {
            echo $e->getErrorMessage();
        } catch (ServerException $e) {
            echo $e->getErrorMessage();
        }
        return null;
    }
}
