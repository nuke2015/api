<?php
namespace nuke2015\api\org;

use nuke2015\api\config;
// "zoujingli/wechat-developer": "^1.2"
use WeChat;

class wxpay
{
    public static function conn()
    {
        $config = config\pay::weixin();
        // 创建接口实例
        $wechat = new \WeChat\Pay($config);
        return $wechat;
    }

    // 微信支付
    public static function wxapp($total_fee, $openid, $title = '小程序支付', $out_trade_no = '')
    {
        // todo,强制付一分
        if ($total_fee < 1) {
            $total_fee = 1;
        }

        $wechat = self::conn();
        $ip     = myhttp::client_ip();
        $config = config\pay::weixin();
        if (!$out_trade_no) {
            $out_trade_no = 'wxpay' . md5(time() . uniqid());
        }

        $options = [
            'body'             => $title,
            'out_trade_no'     => $out_trade_no,
            'total_fee'        => $total_fee,
            'openid'           => $openid,
            'trade_type'       => 'JSAPI',
            // 回调网关
            'notify_url'       => $config['notice_url'],
            'spbill_create_ip' => $ip,
        ];
// var_dump($options);exit;
        try {

            // 生成预支付码
            $result = $wechat->createOrder($options);
            if ($result['result_code'] == 'FAIL') {
                return [2, $result['err_code_des']];
            }

            // 创建JSAPI参数签名
            $options = $wechat->createParamsForJsApi($result['prepay_id']);
            // echo '<pre>';
            // var_dump($result, $options);
            // @todo 把 $options 传到前端用js发起支付就可以了
            return [0, $options];
        } catch (Exception $e) {
            return [1, $e->getMessage()];
        }
    }

    // 微信支付查询
    // ['out_trade_no'=>'']
    public static function wx_query($out_trade_no)
    {
        $wechat = self::conn();
        $result = $wechat->queryOrder(['out_trade_no' => $out_trade_no]);
        if ($result['result_code'] == 'SUCCESS') {
            return [0, $result];
        } else {
            return [1, $result['err_code_des']];

        }
    }

    // 微信支付查询
    // ['out_trade_no'=>'']
    public static function wx_query_by_transaction_id($transaction_id)
    {
        $wechat = self::conn();
        $result = $wechat->queryOrder(['transaction_id' => $transaction_id]);
        if ($result['result_code'] == 'SUCCESS') {
            return [0, $result];
        } else {
            return [1, $result['err_code_des']];

        }
    }

}
