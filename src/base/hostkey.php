<?php

namespace nuke2015\api\base;

// 主机内部参数,唯一指纹
// 注意事项:这个是模拟参数,
// 不同的redis存储主机,会导致数据不一致!
// 它存在的价值,就是为了区分线上本机环境与外部环境.
class hostkey
{

    // 机器指纹,按分钟过期
    public static function only_by_min()
    {
        return self::only_by_time(60);
    }

    // 机器指纹,按小时过期
    public static function only_by_hour()
    {
        return self::only_by_time(3600);
    }

    // 机器指纹,按天过期
    public static function only_by_day()
    {
        return self::only_by_time(86400);
    }

    // 机器指纹,2个月过期
    public static function only_by_month()
    {
        return self::only_by_time(86400 * 60);
    }

    // redis唯一参数,每天换,用于区分不同的主机环境.
    public static function only_by_time($expire)
    {
        $mykey = 'nuke2015/api/base/hostkey/only_by_time/';
        $mykey = md5($mykey . $expire);
        $x     = CacheRedis::get($mykey);
        if (!$x) {
            $rnd = md5(time() . uniqid());
            // 一天过期
            CacheRedis::set($mykey, $rnd, $expire);
            $x = $rnd;
        }
        return $x;
    }
}
